# tamanho padrão do tabuleiro
DEFAULT_SIZE = 3
# X = primeiro
# 0 = segundo

class Grid:
    '''
    Classe que representa o tabuleiro
    '''
    def __init__(self, config, size):
        self._Xmovements = 0    # número de movimentos do X (primeiro)
        self._0movements = 0    # número de movimentos do 0 (segundo)
        self._empty = 0         # movimentos ainda para serem feitos
        self.config = config    # configuração do tabuleiro
        self.size = size        # tamanho
        self.valid = self._validate_grid(config)  # validade do tabuleiro

    ###
    # validações do tabuleiro
    ###

    def _validate_movements(self, config):
        '''
        Válida e contabiliza os movimentos no tabuleiro
        '''
        for i in range(self.size):
                if str.upper(self.config[i]) == 'X':
                    self.config[i] = 'X'
                    self._Xmovements += 1
                elif self.config[i] == 'O':
                    self._0movements += 1
                elif self.config[i] == '.':
                    self._empty += 1
                else:
                    return False
        return True

    def _validate_players(self, config):
        '''
        Válida o número de jogada dos jogadores
        '''
        if abs(self._Xmovements - self._0movements) > 1:
            return False

        return True

    def _validate_grid(self, config):
        '''
        Aplicada todas as validações
        '''
        if self._validate_movements(config) and \
           self._validate_players(config):
            return True
        else:
            return False

    def _next_movement(self):
        '''
         Checa quem é o próximo a jogar
        '''
        if self._Xmovements <= self._0movements:
            return "Primeiro"
        else:
            return "Segundo"

    def _directions(self):
        '''
        Verifica se existe linhas/colunas/diagonais preenchidas com o
        mesmo simbolo caso não ainda há partidas a serem feitas
        '''
        for i in ['X', 'O']:
            # diagonal
            if self.config[0] == self.config[4] == self.config[8] == i:
                return i
            if self.config[6] == self.config[4] == self.config[2] == i:
                return i
            # vertical
            if self.config[0] == self.config[3] == self.config[6] == i:
                return i
            if self.config[1] == self.config[4] == self.config[7] == i:
                return i
            if self.config[2] == self.config[5] == self.config[8] == i:
                return i
            # horizontal
            if self.config[0] == self.config[1] == self.config[2] == i:
                return i
            if self.config[3] == self.config[4] == self.config[5] == i:
                return i
            if self.config[6] == self.config[7] == self.config[8] == i:
                return i

        return -1

    def check(self):
        '''
        Verifica o estado do tabuleiro e volta possíveis valores:
        - inválido se a configuração está errada
        - 'Primeiro/Segundo ganhou' caso haja vitorioso
        - 'Empate' caso o tabuleiro esteja preenchido sem vitória
        - 'Primeiro/Segundo' caso haja movimentos a serem feitos
        '''
        if not self.valid:
            return "Inválido"

        if self._empty == 0:
            # checa se na configuração atual alguém pode vencer
            winner = self._directions()
            if winner == "X":
                return "Primeiro ganhou"
            elif winner == 'O':
                return "Segundo ganhou"
            elif winner == -1:
                return "Empate"
                # não tem vencedor, entaõ ainda da pra jogar ou é empate
        else:
            return self._next_movement()


if __name__ == "__main__":

    grid_config = []
    for _ in range(0, DEFAULT_SIZE):
        grid_config.append(list(input().strip()))

    # lemos como um 2D, entretanto é mais fácil trabalhar com 1D então fazemos um flatten
    grid_config = [val for sublist in grid_config for val in sublist]
    grid = Grid(grid_config, DEFAULT_SIZE*DEFAULT_SIZE)
    print(grid.check())
