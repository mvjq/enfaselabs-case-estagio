from math import ceil
import unittest

# necessidade de redefinir a função
# pois o test case ou roda numa função local ou roda num package
def min_tireddistance(x1, x2):
    '''Função que calcula o ponto de encontro de menor cansaço entre dois
    amigos numa reta.

    Complexidade:

     Tempo: O(c + sum_x1(N) + sum_x2(M)):
     - c: custo constante de alocação das variaveis
     - sum_x1(N): custo de calcular o cansaço em  N passos
     - sum_x2(M): custo de calcular o cansaçao em M passos
     ps: M != N

    Lógica: Pegamos a distância entre dois pontos e pegamos a metade
    dessa distância, o Xa anda até o teto dessa metade enquanto que o
    amigo anda no restante, calculamos o custo dessas duas caminhas e somamos

    '''
    # pegamos a dis
    distance = abs(x1 - x2)
    distance_xa = ceil(distance // 2)
    distance_xb = abs(distance_xa - distance)
    sum_x1 = sum(x for x in range(0, distance_xa+1))
    sum_x2 = sum(x for x in range(0, distance_xb+1))
    return(sum_x1 + sum_x2)


class TestProblem1(unittest.TestCase):

    def test_input1(self):
        self.assertEqual(min_tireddistance(120, 121), 1)

    def test_input2(self):
        self.assertEqual(min_tireddistance(120, 121), 1)

    def test_upperbound1(self):
        self.assertEqual(min_tireddistance(1000, 999), 1)

    def test_upperbound2(self):
        self.assertEqual(min_tireddistance(999, 1000), 1)

    def test_lowerbound1(self):
        self.assertEqual(min_tireddistance(0, 1), 1)

    def test_lowerbound2(self):
        self.assertEqual(min_tireddistance(1, 0), 1)

    def test_maxdistance1(self):
        self.assertEqual(min_tireddistance(0, 1000), 250500)

    def test_maxdistance2(self):
        self.assertEqual(min_tireddistance(1000, 0), 250500)

if __name__ == "__main__":
    unittest.main()
