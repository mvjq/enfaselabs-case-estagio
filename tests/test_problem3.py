import unittest
from functools import lru_cache

def fibonacci_subsequence(seq, length):
    '''
    Função que procura na sequencia <seq> de tamanho <length> a maior subsequencia
    da forma fibonacci (o próximo é a soma dos anteriores)

    fn = fn-2 + fn-1

    Complexidade:
     Tempo:
     - O(N * N * O(1)): N*N pelo loop encadeado e O(1) para acessar a hash


     Memória: considerando apenas as alocações:
     - O(N + c): onde N é hashtable alocada e c uma soma constante do tamanhno das variaveis
    '''

    if not seq or not length:
        raise Exception("Invalid parameters ")

    # criamos uma hashtable com os valores da sequência
    # para termos consulta em O(1)
    hashtable_sequence = {value: value for value in seq}
    max_ocurrence = 0

    # para cada elemento, comparamos com o próximo
    for f in range(length-1):
        for s in range(f+1, length):
            first, second = seq[f], seq[s]
            # assumimos que já temos first + second
            # como uma sequência fibonacci, já contamos como duas ocorrências
            occur_count = 2
            while first + second in hashtable_sequence:
                # olhamos na hash se a soma entre o first e second
                # existe na hash, se sim, contabilizamos
                first, second = second, first+second
                occur_count += 1
                max_ocurrence = max(max_ocurrence, occur_count)

    return max_ocurrence if max_ocurrence >= 2 else 0

def fibonacci_sequence(n):
    '''Função que gera sequência fibonacci até N.
    (usada para auxiliar nos testes)
    '''
    sequence = []
    a, b = 0, 1
    for i in range(0, n):
        a, b = b, a + b
        sequence.append(a)
    return sequence


class TestProblem3(unittest.TestCase):

    def test_input1(self):
        self.assertEqual(fibonacci_subsequence([1, 2, -1], 3), 3)

    def test_input2(self):
        self.assertEqual(fibonacci_subsequence([28, 35, 7, 14, 21], 5), 4)

    def test_input3(self):
        n = fibonacci_sequence(100)
        self.assertEqual(fibonacci_subsequence(n, 100), 100)

    def test_upperbound(self):
        n = fibonacci_sequence(1000)
        self.assertEqual(fibonacci_subsequence(n, 1000), 1000)

    def test_indexerror(self):
        n = fibonacci_sequence(100)
        with self.assertRaises(IndexError):
            fibonacci_subsequence(n, 300)


if __name__ == "__main__":
    unittest.main()
