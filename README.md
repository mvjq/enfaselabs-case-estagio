# Processo Seletivo Enfase Labs

    Soluções do processo seletivo para Enfâse Labs 2019/2.

## Orientações

    As soluções foram feitas em Python 3.7.

    Estrutura do diretório:

    +-- problem1.py
    +-- problem2.py
    +-- problem3.py
    +-- inputs/
    |  +-- input1.problem1
    |  +-- input2.problem1
    |  +-- input1.problem2
    |  +-- input2.problem2
    |  +-- input1.problem3
    |  +-- input2.problem3
    +-- tests/
    |  +-- test_problem1.py
    |  +-- test_problem2.py
    |  +-- test_problem3.py


    Para executar com os inputs do pdf do problema:

    > python problem1.py < inputs/input1.problem1

    Para executar com input arbitrário

    > python problem1.py

### Testes

    Para executar todos os testes do problema 1, faça:

    > python tests/test_problem1.py -v



### Author

    Marcos Vinicius Junqueira
    email: marcos.junqueira@usp.br
