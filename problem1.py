from math import ceil


def min_tireddistance(x1, x2):
    '''Função que calcula o ponto de encontro de menor cansaço entre dois
    amigos numa reta.

    Complexidade:

     Tempo: O(c + sum_x1(N) + sum_x2(M)):
     - c: custo constante de alocação das variaveis
     - sum_x1(N): custo de calcular o cansaço em  N passos
     - sum_x2(M): custo de calcular o cansaçao em M passos
     ps: M != N

    Lógica: Pegamos a distância entre dois pontos e pegamos a metade
    dessa distância, o Xa anda até o teto dessa metade enquanto que o
    amigo anda no restante, calculamos o custo dessas duas caminhas e somamos

    '''
    # pegamos a dis
    distance = abs(x1 - x2)
    distance_xa = ceil(distance // 2)
    distance_xb = abs(distance_xa - distance)
    sum_x1 = sum(x for x in range(0, distance_xa+1))
    sum_x2 = sum(x for x in range(0, distance_xb+1))
    print(sum_x1 + sum_x2)


if __name__ == "__main__":
    x1 = int(input())
    x2 = int(input())
    min_tireddistance(x1, x2)
